package org.kirillbogatikov.fraction_expression;

/**
 * Класс, представляющий дробь, в т.ч. обыкновенную, десятичную дроби, а также целые числа 
 * 
 * @author Кирилл Исполньов, 16ИТ18К
 */
public class Fraction extends Number {
    /**
     * В языке Java (да, ив некоторых других) есть чудесная вещь - сериализация. 
     * Эта возможность позволяет программисту сохранять состояние объекта в файл, как будто это строка или число.
     * В файл сохраняются все поля объекта и значения этих полей. При следующем заупске программы Вы сможете восстановить состояние объекта из файла
     * с помощью обратного процесса - ДЕсериализации
     * 
     * Для того, чтобы понимать экземпляру какого класса принадлежат данные из файла созданы идентификаторы, описанные в реализациях этих самих классов. 
     * Пример снизу
     */
    private static final long serialVersionUID = 5300754298847240169L;
    
    /**
     * Метод-фабрика. Производит парсинг (преобразование) данных из "сырого" вида (строка) в объект Fraction
     * В обработке для определния, в какой же форме представлены данные используются реуглярные выражения
     * 
     * @param input строка, содержащая дробь или число
     * @return дробь, полученная из строки input
     */
    public static Fraction parse(String input) {
        //если стока содрежит только числа, то одноначно это целое число
        if(input.matches("[+-]?\\d+")) {
            return new Fraction(Integer.parseInt(input), 1);
        }
        
        //если строка содержит несколько чисел, разделённых точкой, то это десятичная дробь
        if(input.matches("[+-]?(\\d+)\\.(\\d+)")) {
            String[] parts = input.split("\\.");
            //целая часть
            int a = Integer.parseInt(parts[0]);
            //дробная часть
            int b = Integer.parseInt(parts[1]);
            //делитель дробной части, как степень числа 10
            int c = (int)Math.pow(10, parts[1].length());
            
            //преобразование для дальнейшей обработки
            input = a + "(" + b + "/" + c + ")";   
        }
        
        int s = 1;
        int n = 0, d;
        
        //если строка содержит обычную дробь с выделенной целой частью
        //то нам необходимо сначала внести целую часть в числитель дроби, а затем уже производить обработку
        if(input.matches("[+-]?\\d+\\((\\d+)/(\\d+)\\)")) {
            String wholePart = input.substring(0, input.indexOf("("));
            n = Integer.parseInt(wholePart);
            s = n < 0 ? -1 : 1;
            input = input.substring(input.indexOf("(") + 1, input.length() - 1);
        }
        
        //парсинг обыкновенной дроби 
        if(input.matches("[+-]?(\\d+)/(\\d+)")) {
            d = Integer.parseInt(input.substring(input.indexOf("/") + 1, input.length()));
            n = n == 0 ? 0 : n * d;
            n += s * Integer.parseInt(input.substring(0, input.indexOf("/")));
            return new Fraction(n, d);
        }
        
        //выполнение этого кода возможно лишь в случае, если исходная строка ни одному шаблону не подошла
        throw new RuntimeException("Unknown format: " + input);
            
    }
    
    //знаменатель и числитель
    private int denominator, numerator;
    
    /**
     * Конструктор, инциализирующий объект необходимыми данными
     *  
     * @param n числитель дроби
     * @param d знаменатель дроби
     * @throws ArithmeticException данное исключение будет выброшено, если знаменатель дроби есть 0
     */
    public Fraction(Number a, Number b) {
        if(a instanceof Integer && b instanceof Integer) {
            this.numerator = a.intValue();
            this.denominator = b.intValue();
        } else {
            float n = a.floatValue();
            float d = b.floatValue();
            
            
            int k = 10;
            while(n % 1 != 0 || d % 1 != 0) {
                n *= k;
                d *= k;
                k *= 10;
            }
            
            this.numerator = (int)n;
            this.denominator = (int)d;
        }
        
        //если знаменатель дроби - 0
        if(this.denominator == 0) {
            //если числитель тоже равен 0
            if(this.numerator == 0) {
                //сохраняем дробь, представляющую 0 как 0/1
                this.denominator = 1;
            } else {
                //ааааа, нельзя допустить ситуацию, чтобы знаменатлеь дроби был равен 0!
                throw new ArithmeticException("Знаменатель дроби не может быть нулём! (На ноль делить нельзя)");
            }
        }
        
        simplify();
    }
    
    public Fraction() {
        this(0, 1);
    }
    
    /**
     * Существование метода избыточно. Единственный аргумент в польззу его существования - обеспечение независимости от класса вычислений {@link Arithmetic}
     * Возвращает НОД двух чисел
     * 
     * @param a число 
     * @param b число
     * @return НОД двух чисел
     */
    private int getGCD(int a, int b) {
        return b != 0 ? getGCD(b, a % b) : a;
    }
    
    /**
     * упрощает дробь, выполняя деление числителя и знаменателя на их НОД
     */
    public void simplify() {
        int gcd = getGCD(Math.abs(numerator), Math.abs(denominator));
        numerator /= gcd;
        denominator /= gcd;
    }
    
    /**
     * Возвращает значение дроби в виде числа double  
     */
    @Override
    public double doubleValue() {
        return (double)numerator / denominator;
    }

    /**
     * Возвращает значение дроби в виде числа float
     */
    @Override
    public float floatValue() {
        return (float)doubleValue();
    }

    /**
     * Вовзращает целую часть дроби
     */
    @Override
    public int intValue() {
        return numerator / denominator;
    }

    /**
     * Возвращает целую часть дроби
     */
    @Override
    public long longValue() {
        return (long)intValue();
    }

    /**
     * Возвращает знаменатель дроби
     * 
     * @return знаменатель дроби
     */
    public int getDenominator() {
        return denominator;
    }

    /**
     * Устанавливает знаменатель дроби
     * 
     * @param denominator новый знаменатель дроби
     */
    @Deprecated
    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    /**
     * Возвращает числитель дроби
     * 
     * @return числитель дроби
     */
    public int getNumerator() {
        return numerator;
    }

    /**
     * Утсанавливает числитель дроби
     *  
     * @param numerator числитель дроби
     */
    @Deprecated
    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }
    
    /**
     * Определяет, является ли число, представленное дробью нулём
     * 
     * @return true, если дробь есть 0/1
     */
    public boolean isZero() {
        return numerator == 0 && denominator == 1;
    }
    
    /**
     * Переворачивает дробь
     * Возвращает дробь, числитель которой есть знаменатель текущей, а знаменатель - числитель текущей.
     * 
     * @return перевёрнутая дробь
     */
    public Fraction getReverse() {
        if(isZero())
            return new Fraction(numerator, denominator);
        
        return new Fraction(denominator, numerator);
    }
    
    @Override
    public String toString() {
        int n = this.numerator, d = this.denominator;
        double whole = (double)n / d;
        
        if(whole % 1.0 == 0.0) {
            return Integer.toString((int)whole);
        }
        
        String decFraction = Double.toString(whole);
        
        if(decFraction.length() - decFraction.indexOf(".") < 4) {
            return decFraction; 
        }
        
        StringBuilder builder = new StringBuilder();
        
        if((int)whole != 0) {
            builder.append((int)whole);
        }
        
        builder.append("(").append(n % d).append("/").append(d).append(")");
        return builder.toString();
    }
}
