package org.kirillbogatikov.fraction_expression.easy;

/**
 * Класс, представляющий дробь, в т.ч. обыкновенную, десятичную дроби, а также целые числа 
 * 
 * @author Кирилл Исполньов, 16ИТ18К
 */
public class Fraction {
    //знаменатель и числитель
    private int denominator, numerator;
    
    /**
     * Конструктор, инциализирующий объект необходимыми данными
     *  
     * @param numerator числитель дроби
     * @param denominator знаменатель дроби
     */
    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        simplify();
    }
    
    public Fraction(int whole, int numerator, int denominator) {
    	this(numerator + whole * denominator, denominator);
    }
    
    public Fraction() {
        this(0, 1);
    }
    
    /**
     * Существование метода избыточно. Единственный аргумент в пользу его существования - обеспечение независимости от класса вычислений {@link Arithmetic}
     * Возвращает НОД двух чисел
     * 
     * @param a число 
     * @param b число
     * @return НОД двух чисел
     */
    private int getGCD(int a, int b) {
        return b != 0 ? getGCD(b, a % b) : a;
    }
    
    /**
     * упрощает дробь, выполняя деление числителя и знаменателя на их НОД
     */
    public void simplify() {
        int gcd = getGCD(Math.abs(numerator), Math.abs(denominator));
        numerator /= gcd;
        denominator /= gcd;
    }
    
    /**
     * Возвращает значение дроби в виде числа double  
     */
    public double doubleValue() {
        return (double)numerator / denominator;
    }

    /**
     * Вовзращает целую часть дроби
     */
    public int intValue() {
        return numerator / denominator;
    }

    /**
     * Возвращает знаменатель дроби
     * 
     * @return знаменатель дроби
     */
    public int getDenominator() {
        return denominator;
    }

    /**
     * Возвращает числитель дроби
     * 
     * @return числитель дроби
     */
    public int getNumerator() {
        return numerator;
    }

    /**
     * Определяет, является ли число, представленное дробью нулём
     * 
     * @return true, если дробь есть 0/1
     */
    public boolean isZero() {
        return numerator == 0 && denominator == 1;
    }
    
    /**
     * Переворачивает дробь
     * Возвращает дробь, числитель которой есть знаменатель текущей, а знаменатель - числитель текущей.
     * 
     * @return перевёрнутая дробь
     */
    public Fraction getReverse() {
        if(isZero())
            return new Fraction(numerator, denominator);
        
        return new Fraction(denominator, numerator);
    }
    
    @Override
    public String toString() {
        int n = this.numerator, d = this.denominator;
        double whole = (double)n / d;
        
        if(whole % 1.0 == 0.0) {
            return Integer.toString((int)whole);
        }
        
        String decFraction = Double.toString(whole);
        
        if(decFraction.length() - decFraction.indexOf(".") < 4) {
            return decFraction; 
        }
        
        StringBuilder builder = new StringBuilder();
        
        if((int)whole != 0) {
            builder.append((int)whole);
        }
        
        builder.append("(").append(n % d).append("/").append(d).append(")");
        return builder.toString();
    }
}
