package org.kirillbogatikov.fraction_expression.easy;

/**
 * Предоставляет набор методов, реализующих некоторые арифметические действия над числами:
 * <ul>
 *  <li>{@link #sum(Fraction...)} Сумма 2 и более дробей</li>
 *  <li>{@link #sub(Fraction...)} Разность 2 и более дробей</li>
 *  <li>{@link #multiple(Fraction...)} Произведение 2 и более дробей</li>
 *  <li>{@link #divide(Fraction...)} Частное 2 и более дробей</li>
 *  <li>{@link #getNumbersGCD(int, int)} Нахождение НОД - наибольшего общего знаменателя</li>
 *  <li>{@link #getNumbersLCM(int, int)} Нахождение НОК - наименьшего общего кратного</li>
 *  <li>{@link #getFractionsGCD(Fraction...)} Нахождение НОД для 2 и более дробей</li>
 *  <li>{@link #getFractionsLCM(Fraction...)} Нахождение НОК для 2 и более дробей</li>
 * </ul>
 */
public class Arithmetic {
    /**
     * Текст сообщения, выводимого с ошибкой при попытке деления на ноль
     */
    private static final String DIVIDING_BY_ZERO = "Делить на нуль нельзя!";
    
    /**
     * Возвращает сумму дробей 
     * 
     * @param a дробь
     * @param b дробь     
     * @return сумма дробей
     */
    public static Fraction sum(Fraction a, Fraction b) {
       int denominator = getLCM(a.getDenominator(), b.getDenominator());
       int numerator = a.getNumerator() * (denominator / a.getDenominator());
       numerator += b.getNumerator() * (denominator / b.getDenominator());
       
       return new Fraction(numerator, denominator);
    }
    
    /**
     * Возвращает разность дробей 
     * 
     * @param a дробь
     * @param b дробь     
     * @return разность дробей
     */
    public static Fraction sub(Fraction a, Fraction b) {
        int denominator = getLCM(a.getDenominator(), b.getDenominator());
        int numerator = a.getNumerator() * (denominator / b.getDenominator());
        numerator -= b.getNumerator() * (denominator / b.getDenominator());
        
        return new Fraction(numerator, denominator);
    }
    
    /**
     * Возвращает произведение нескольких дробей 
     * 
     * @param a дробь
     * @param b дробь
     * @return произведение дробей
     */
    public static Fraction multiple(Fraction a, Fraction b) {
        int numerator = a.getNumerator() * b.getNumerator();
        int denominator = a.getDenominator() * b.getDenominator();
        
    	return new Fraction(numerator, denominator);
    }
    
    /**
     * Возвращает произведение нескольких дробей 
     * 
     * @param a дробь
     * @param b дробь
     * @return частное дроби a
     * @throws Exception 
     */
    public static Fraction divide(Fraction a, Fraction b) throws Exception {
    	if(b.isZero()) {
    		throw new Exception(DIVIDING_BY_ZERO);
    	}
    	
        return multiple(a, b.getReverse());
    }
    
    public static Fraction pow(Fraction a, Fraction b) {
        double grade = b.doubleValue();
        double numerator = Math.pow(a.getNumerator(), grade);
        double denominator = Math.pow(a.getDenominator(), grade);
         
        return new Fraction((int)numerator, (int)denominator);
    }
    
    /**
     * Возвращает наибольший общий делитель двух чисел
     * Применяется рекурсивная версия алгоритма Евклида
     * 
     * @param a первое число
     * @param b второе число
     * @return наибольший общий делитель
     */
    public static int getGCD(int a, int b) {
        return b != 0 ? getGCD(b, a % b) : a;
    }
    
    /**
     * Возвращает наименьшее общее кратное двух чисел
     * 
     * @param a первое число
     * @param b второе число
     * @return наименьшее общее кратное двух чисел
     */
    public static int getLCM(int a, int b) {
        return Math.abs(a * b) / getGCD(a, b);
    }
}
