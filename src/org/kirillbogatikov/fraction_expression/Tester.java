package org.kirillbogatikov.fraction_expression;

import java.util.Scanner;

public class Tester {
    static Scanner scanner = new Scanner(System.in);
    static String[] SYMBOLS = { "+", "-", "*", ":", "^"};
    
    public static void main(String[] args) {
        System.err.println(new Fraction(1, 5));
        
        String expression = scanner.nextLine().replaceAll(" +", "");
        process(expression);
    }

    public static void process(String expression) {
        String f1 = null, s = null, f2 = null;
        
        for(String sym : SYMBOLS) {
            int i = expression.indexOf(sym); 
            if(i != -1) {
                f1 = expression.substring(0, i);
                s = expression.substring(i, ++i);
                f2 = expression.substring(i);
            }
        }
        
        if(f1 == null) {
            System.err.println("Не удалось распознать выражение");
        }
        
        process(f1, s, f2);
    }
    
    public static void process(String f1, String s, String f2) {
        Fraction a = Fraction.parse(f1);
        Fraction b = Fraction.parse(f2);
        Fraction c = null;
        
        if(s.equals(SYMBOLS[0])) {
            c = Arithmetic.sum(a, b);
        } else if(s.equals(SYMBOLS[1])) {
            c = Arithmetic.sub(a, b);
        } else if(s.equals(SYMBOLS[2])) {
            c = Arithmetic.multiple(a, b);
        } else if(s.equals(SYMBOLS[3])) {
            c = Arithmetic.divide(a, b);
        } else if(s.equals(SYMBOLS[4])) {
            c = Arithmetic.pow(a, b);
        }
        
        System.out.println(c.getNumerator() + "/" + c.getDenominator());
    }
}
