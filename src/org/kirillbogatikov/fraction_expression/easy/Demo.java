package org.kirillbogatikov.fraction_expression.easy;

import java.util.Scanner;

public class Demo {
    static Scanner scanner = new Scanner(System.in);
    static String[] SYMBOLS = { "+", "-", "\\", "\\*", "^" };
    
    public static void main(String[] args) throws Exception {
        String line = scanner.nextLine();
        
        int mode = 0, i = 0;
        for(; mode < SYMBOLS.length; mode++) {
        	i = line.indexOf(SYMBOLS[mode]);
        	if(i != -1) break;
        }
        
        Fraction a = parse(line.substring(0, i));
        Fraction b = parse(line.substring(i + 1));
        
        Fraction c;
        switch(mode) {
        	case 0: c = Arithmetic.sum(a, b); break;
        	case 1: c = Arithmetic.sub(a, b); break;
        	case 2: c = Arithmetic.divide(a, b); break;
        	case 3: c = Arithmetic.multiple(a, b); break;
        	case 4: c = Arithmetic.pow(a, b); break;
        	default: System.out.println("Операция не распознана"); return;
        }
        System.out.println(c);
    }

    public static Fraction parse(String f) {
    	int whole = 0;
    	
    	int i = f.indexOf("(");
    	int j = f.indexOf("/");
    	if(i != -1) {
    		whole = Integer.parseInt(f.substring(0, i));
    	}
    	
    	int numerator = Integer.parseInt(f.substring(i + 1, j));
    	int denominator = Integer.parseInt(f.substring(j+1, f.length() - (i == -1 ? 0 : 1)));
    	
    	return new Fraction(whole, numerator, denominator);
    }
}
