package org.kirillbogatikov.fraction_expression;

/**
 * Предоставляет набор методов, реализующих некоторые арифметические действия над числами:
 * <ul>
 *  <li>{@link #sum(Fraction...)} Сумма 2 и более дробей</li>
 *  <li>{@link #sub(Fraction...)} Разность 2 и более дробей</li>
 *  <li>{@link #multiple(Fraction...)} Произведение 2 и более дробей</li>
 *  <li>{@link #divide(Fraction...)} Частное 2 и более дробей</li>
 *  <li>{@link #getNumbersGCD(int, int)} Нахождение НОД - наибольшего общего знаменателя</li>
 *  <li>{@link #getNumbersLCM(int, int)} Нахождение НОК - наименьшего общего кратного</li>
 *  <li>{@link #getFractionsGCD(Fraction...)} Нахождение НОД для 2 и более дробей</li>
 *  <li>{@link #getFractionsLCM(Fraction...)} Нахождение НОК для 2 и более дробей</li>
 * </ul>
 * 
 * @author Кирилл �?спольнов, 16�?Т18К
 */
public class Arithmetic {
    /**
     * Текст сообщения, выводимого с ошибкой при попытке деления на ноль
     */
    private static final String DIVIDING_BY_ZERO = "Делить на нуль нельзя!";
    /**
     * Текст сообщения, выводимого с ошибкой при попытке вызвать метод, требующий 2 и более аргументов с меньшим числом входных переменных
     */
    private static final String ARGS_COUNT_MUST_BE_BIGGER_THAN_ONE = "Число аргументов метода должно быть более одного!";

    /**
     * Возвращает сумму нескольких дробей. Вызов метода завершится выбросом ошибки при попытке вызвать его с 1 и менее аргументами 
     * 
     * Алгоритм:
     * 1. Вычисляется НОК всех дробей. Это значение использщауется в качестве общего знаменателя
     * 2. Вычисляется число K - отношение общего знаменателя к знаменателю конкретной дроби
     * 3. Вычисляется сумма числителей всех дробей, умноженных на число K. 
     * 
     * @param fs Несколько дробей перечисляемых с помощью щапятой и переданных в массиве     
     * @return сумма дробей
     * @throws ArithmeticException метод сможет найти сумму 2 и более дробей. Если аргументов будет передано меньше будет выброшено сие исключение
     */
    public static Fraction sum(Fraction... fs) throws ArithmeticException {
        if(fs.length < 2) {
            throw new ArithmeticException(ARGS_COUNT_MUST_BE_BIGGER_THAN_ONE);
        }
        
        int numerator = 0, denominator = getFractionsLCM(fs);
        for(Fraction f : fs) {
            numerator += f.getNumerator() * (denominator / f.getDenominator());
        }
        return new Fraction(numerator, denominator);
    }
    
    /**
     * Возвращает разность нескольких дробей. Вызов метода завершится выбросом ошибки при попытке вызвать его с 1 и менее аргументами 
     * 
     * Алгоритм:
     * 1. Вычисляется НОК всех дробей. Это значение будет использовано в качестве общего знаменателя дробей.
     * 2. Вычисялется число К0 - отношение общего знаменателя дробей к знаменателю первой дроби.
     * 3. Числитель первой дроби умножается на К0, т.о. вычисляется число N
     * 4. По очередно для каждой дроби вычисляется число К
     * 5. �?з числа N вычитается числитель текущей дроби умноженный на К
     * 
     * @param fs Несколько дробей перечисляемых с помощью щапятой и переданных в массиве     
     * @return разность дробей
     * @throws ArithmeticException метод сможет найти сумму 2 и более дробей. Если аргументов будет передано меньше будет выброшено сие исключение
     */
    public static Fraction sub(Fraction... fs) throws ArithmeticException {
        if(fs.length < 2) {
            throw new ArithmeticException(ARGS_COUNT_MUST_BE_BIGGER_THAN_ONE);
        }
        
        int denominator = getFractionsLCM(fs), numerator = fs[0].getNumerator() * (denominator / fs[0].getDenominator());
        Fraction f;
        for(int i = 1; i < fs.length; i++) {
            f = fs[i];
            numerator -= f.getNumerator() * (denominator / f.getDenominator());
        }
        return new Fraction(numerator, denominator);
    }
    
    /**
     * Возвращает произведение нескольких дробей. Вызов метода завершится выбросом ошибки при попытке вызвать его с 1 и менее аргументами 
     * 
     * Алгоритм:
     * 1. Вычисялется произведение числителей всех дробей
     * 2. Вычисляется произведение знаменателей всех дробей 
     * 
     * @param fs Несколько дробей перечисляемых с помощью щапятой и переданных в массиве
     * @return произведение дробей
     * @throws ArithmeticException метод сможет найти сумму 2 и более дробей. Если аргументов будет передано меньше будет выброшено сие исключение
     */
    public static Fraction multiple(Fraction... fs) throws ArithmeticException {
        if(fs.length < 2) {
            throw new ArithmeticException(ARGS_COUNT_MUST_BE_BIGGER_THAN_ONE);
        }
        
        int numerator = 1, denominator = 1;
        for(Fraction f : fs) {
            numerator *= f.getNumerator();
            denominator *= f.getDenominator();
        }
        return new Fraction(numerator, denominator);
    }
    
    /**
     * Возвращает произведение нескольких дробей. Вызов метода завершится выбросом ошибки при попытке вызвать его с 1 и менее аргументами 
     * 
     * Алгоритм:
     * 1. В переменной Q сохраняется значение числителя первой дроби.
     * 2. На число Q поочередно умножаются знаменатели всех остальных дробей.
     * 3. Вычисялется произведние знаменателя первой дроби и числителей всех остальных дробей.
     * 
     * @param fs Несколько дробей перечисляемых с помощью щапятой и переданных в массиве
     * @return частное нескольких дробей
     * @throws ArithmeticException метод сможет найти сумму 2 и более дробей. Если аргументов будет передано меньше будет выброшено сие исключение
     */
    public static Fraction divide(Fraction... fs) throws ArithmeticException {
        if(fs.length < 2) {
            throw new ArithmeticException(ARGS_COUNT_MUST_BE_BIGGER_THAN_ONE);
        }
        
        Fraction quotient = fs[0];
        for(int i = 1; i < fs.length; i++) {
            if(fs[i].isZero()) {
                throw new ArithmeticException(DIVIDING_BY_ZERO);
            }
            
            quotient = multiple(quotient, fs[i].getReverse());
        }
        return quotient;
    }
    
    public static Fraction pow(Fraction a, Fraction b) {
        double grade = b.doubleValue();
        double numerator = Math.pow(a.getNumerator(), grade);
        double denominator = Math.pow(a.getDenominator(), grade);
         
        return new Fraction(numerator, denominator);
    }
    
    /**
     * Возвращает наибольший общий делитель двух чисел
     * Применяется рекурсивная версия алгоритма Евклида
     * 
     * @param a первое число
     * @param b второе число
     * @return наибольший общий делитель
     */
    public static int getNumbersGCD(int a, int b) {
        return b != 0 ? getNumbersGCD(b, a % b) : a;
    }
    
    /**
     * Возвращает наибольший общий знаменатель нескольких дробей
     * если передано всего дроби, то метод вернёт результаты работы метода {@link #getNumbersGCD(int, int)} для знаменателей этих двух дробей
     * В ином случае сначала будет найден общий знаменатель двух первых дробей (пусть, это будет число B), затем - НОД числа B и знаменателя третьей дроби (число C), затем
     * числа C и знаменателя четвертой дроби и так до последней дроби. 
     * 
     * @param fs несколько дробей, перечисляемых с помощью запятых или переданных в массиве
     * @return наибольший общий знаменатель нескольких дробей
     */
    public static int getFractionsGCD(Fraction... fs) {
        if(fs.length == 2)
            return getNumbersGCD(fs[0].getDenominator(), fs[1].getDenominator());
        
        int b = fs[0].getDenominator();
        for(int i = 1; i < fs.length; i++) {
            b = getNumbersGCD(fs[i].getDenominator(), b);
        }
        return b;
    }
    
    /**
     * Возвращает наименьшее общее кратное двух чисел
     * 
     * @param a первое число
     * @param b второе число
     * @return наименьшее общее кратное двух чисел
     */
    public static int getNumbersLCM(int a, int b) {
        return Math.abs(a * b) / getNumbersGCD(a, b);
    }
    
    /**
     * Возвращает наименьшее общее кратное нескольких дробей
     * Прицнип работы метода схож с принципом метода {@link #getFractionsGCD(Fraction...)}, однако хдесь каждую итерацию вычислется не НОД, а НОК
     * 
     * @param fs несколько дробей, перечисленных с помощью запятых или переданных в массиве
     * @return НОК нескольких дробей
     */
    public static int getFractionsLCM(Fraction... fs) {
        if(fs.length == 2)
            return getNumbersLCM(fs[0].getDenominator(), fs[1].getDenominator());
        
        int b = fs[0].getDenominator();
        for(int i = 1; i < fs.length; i++) {
            b = getNumbersLCM(fs[i].getDenominator(), b);
        }
        return b;
    }
}
